#### Tarea Actualizacion Tecnologica:
-  Realice una aplicacion JAVA REST a la cual hay que pasarle la URL de la base de datos por parametro para que levante, si paso una incorrecta no funcionara (esto demuestra que la app la toma) (el .jar debera estar junto al dockerfile para que lo tome al construir imagen)
-  Bindea logs de contenedor con carpeta de host asi quedan persistidos
-  Proxy inverso nginx contra contenedor en puerto 8080 (VIRTUAL_HOST y VIRTUAL_PORT)


###### Pasos a seguir:

1. docker build -t  guido/basket .   
2. docker run -d -p 80:80  -v /var/run/docker.sock:/tmp/docker.sock:ro  jwilder/nginx-proxy
3. docker run -d -e VIRTUAL_HOST=sitioprueba.test -e VIRTUAL_PORT=8080 -e DB_URL=databasehispano.cb2rl2btechr.us-east-2.rds.amazonaws.com --mount source=myvol2,target=/tmp guido/basket
(En este caso tengo que agregarle el parametro VIRTUAL_PORT=8080 ya que la aplicacion escucha ahi)
4. Modificar /etc/hosts apuntando IP local a sitioprueba.test
5. docker volume inspect myvol2   
(muestra /var/lib/docker/volumes/myvol2/_data que tendra persistidos los datos de los logs que se escriben en /tmp)
6. postman, curl o get en navagador a http://sitioprueba.test o http://sitioprueba.test/news

