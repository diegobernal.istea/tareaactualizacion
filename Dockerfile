FROM java:8-jdk-alpine
#copio .jar de donde estoy parado en carpeta de container
COPY basketbackend-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
#define url de base de datos, que luego podre recibir tambien por parametro
ENV DB_URL localhost
RUN sh -c 'touch basketbackend-0.0.1-SNAPSHOT.jar'

COPY entrypoint.sh /
#doy permisos al script de ejecucion
RUN chmod +x /entrypoint.sh
#ejecuto script
ENTRYPOINT ["/entrypoint.sh"]

